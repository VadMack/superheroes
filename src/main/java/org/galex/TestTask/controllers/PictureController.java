package org.galex.TestTask.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.galex.TestTask.services.PictureService;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/v1/public/pictures")
@RequiredArgsConstructor
public class PictureController {

    private final PictureService pictureService;

    @Operation(
            summary = "Set picture for character",
            description = "Allows upload a jpeg file for character specified by id"
    )
    @RequestMapping(value = "/character", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "404",
                    description = "Character not found"
            ),
            @ApiResponse(
                    responseCode = "415",
                    description = "Provided file is not a jpeg"
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Error occurred while saving picture"
            ),
            @ApiResponse(
                    responseCode = "200",
                    description = "Picture saved"
            ),
            @ApiResponse(
                    responseCode = "422",
                    description = "File is too big"
            )
    }
    )
    public ResponseEntity<String> addCharPicture(@RequestParam("picture") MultipartFile picture,
                                                 @RequestParam("id") String charId,
                                                 HttpServletRequest httpServletRequest) {
        return pictureService.handleCharPicture(picture, charId, httpServletRequest);
    }

    @RequestMapping(value = "/comic", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "404",
                    description = "Comic not found"
            ),
            @ApiResponse(
                    responseCode = "415",
                    description = "Provided file is not a jpeg"
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Error occurred while saving picture"
            ),
            @ApiResponse(
                    responseCode = "200",
                    description = "Picture saved"
            ),
            @ApiResponse(
                    responseCode = "422",
                    description = "File is too big"
            )
    }
    )
    public ResponseEntity<String> addComicPicture(@RequestParam("picture") MultipartFile picture,
                                                  @RequestParam("id") String comicId,
                                                  HttpServletRequest httpServletRequest) {
        return pictureService.handleComicPicture(picture, comicId, httpServletRequest);
    }

    @RequestMapping(value = "/character/{id}", method = RequestMethod.GET, produces = "multipart/form-data")
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "404",
                    description = "Character or picture not found"
            ),
            @ApiResponse(
                    responseCode = "200",
                    description = "Picture found"
            )
    }
    )
    public ResponseEntity<Resource> loadCharPicture(@PathVariable("id") String id) {
        return pictureService.getCharPicture(id);
    }

    @RequestMapping(value = "/comic/{id}", method = RequestMethod.GET, produces = "multipart/form-data")
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "404",
                    description = "Comic or picture not found"
            ),
            @ApiResponse(
                    responseCode = "200",
                    description = "Picture found"
            )
    }
    )
    public ResponseEntity<Resource> loadComicPicture(@PathVariable("id") String id) {
        return pictureService.getComicPicture(id);
    }
}
