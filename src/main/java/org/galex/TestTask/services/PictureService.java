package org.galex.TestTask.services;

import lombok.RequiredArgsConstructor;
import org.galex.TestTask.models.CharacterEntity;
import org.galex.TestTask.models.ComicEntity;
import org.galex.TestTask.repositories.CharacterRepository;
import org.galex.TestTask.repositories.ComicRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@RequiredArgsConstructor
@Service
public class PictureService {

    @Value("${media.characters-pictures-path}")
    private String charPicPath;
    @Value("${media.comics-pictures-path}")
    private String comicPicPath;

    private final ComicRepository comicRepository;
    private final CharacterRepository characterRepository;

    public ResponseEntity<String> handleCharPicture(MultipartFile picture, String id,
                                                    HttpServletRequest httpServletRequest) {
        if (!characterRepository.existsById(id)) {
            return new ResponseEntity<>("Character not found", HttpStatus.NOT_FOUND);
        }
        return savePicture(picture, charPicPath, id, httpServletRequest);
    }

    public ResponseEntity<String> handleComicPicture(MultipartFile picture, String id,
                                                     HttpServletRequest httpServletRequest) {
        if (!comicRepository.existsById(id)) {
            return new ResponseEntity<>("Comic not found", HttpStatus.NOT_FOUND);
        }
        return savePicture(picture, comicPicPath, id, httpServletRequest);
    }

    public ResponseEntity<Resource> getCharPicture(String id){
        if (!characterRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return getPicture(id, charPicPath);
    }

    public ResponseEntity<Resource> getComicPicture(String id){
        if (!comicRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return getPicture(id, comicPicPath);
    }

    private ResponseEntity<String> savePicture(MultipartFile picture, String path,
                                               String id, HttpServletRequest httpServletRequest) {
        if (Objects.equals(picture.getContentType(), "image/jpeg")) {
            Path fullPath = Paths.get(path + "/" + id + ".jpg");
            String servAddr = httpServletRequest.getScheme() + "://" + httpServletRequest.getServerName()
                    + ":" + httpServletRequest.getServerPort();
            try {
                if (!Files.exists(Paths.get(path))) {
                    Files.createDirectories(Paths.get(path));
                }
                picture.transferTo(fullPath);
            } catch (IOException e) {
                return new ResponseEntity<>("Error occurred while saving file", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (path.equals(charPicPath)) {
                CharacterEntity characterEntity = characterRepository.findById(id).get();
                characterEntity.setThumbnail(servAddr + "/v1/public/pictures/character/" + id);
                characterRepository.save(characterEntity);
            } else {
                ComicEntity comicEntity = comicRepository.findById(id).get();
                comicEntity.setThumbnail(servAddr + "/v1/public/pictures/comic/" + id);
                comicRepository.save(comicEntity);
            }
            return new ResponseEntity<>("Picture added", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("File is not a jpeg", HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
    }

    private ResponseEntity<Resource> getPicture(String id, String path){
        Path fullPath = Paths.get(path + "/" + id + ".jpg");
        if(!Files.exists(fullPath)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        try {
            Resource resource = new UrlResource(fullPath.toUri());
            return ResponseEntity.ok()
                    .contentType(MediaType.valueOf("image/jpeg"))
                    .body(resource);
        } catch (MalformedURLException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
