FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app
COPY build/libs/TestTask.jar TestTask.jar
ENTRYPOINT ["java","-jar","/app/TestTask.jar"]
